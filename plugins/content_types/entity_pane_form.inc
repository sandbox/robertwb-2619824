<?php
/**
 * @file
 * Pane for multiple data layer leaflet maps.
 */

$plugin = array(
  'title' => t('Default Entity Panel Form'),
  'description' => t('Display an Entity form in a panel page.'),
  'module' => 'entity_pane_form',
  'category' => t('Entity Pane Forms'),
  'single' => TRUE,
  'defaults' => array(),
  'admin info' => 'entity_pane_form_admin_info',
  'render callback' => 'entity_pane_form_render',
  'edit form' => 'entity_pane_form_edit_form',
  'all contexts' => TRUE, 
);

/**
 * Callback for page_manager admin UI information.
 */
function entity_pane_form_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';

    $admin_info = t('@entity_type (@formid)', array(
      '@entity_type' => $conf['entity_pane_form_settings']['entity_type'],
      '@formid' => $conf['entity_pane_form_settings']['formid'],
    ));
    $block->content = $admin_info;

    return $block;
  }
}

/**
 * Callback to render the entity add/edit form in a pane.
 */
function entity_pane_form_render($subtype, $conf, $panels_args, $context) {
  // Build the pane.
  $pane = new stdClass();
  $pane->module = 'ctools';
  // This title can/should be overriden in the page manager UI.
  $pane->title = 'Entity Pane Form';

  $pane->content = entity_pane_form_render_form($conf, $context);

  return $pane;
}

/**
 * Helper function to generate the form markup based on the pane config.
 */
function entity_pane_form_render_form($conf, $context) {
  $form_state = array();
  $form_state['values'] = array();
  // extract info about the root entity of the form
  $base_conf = $conf['entity_pane_form_settings'];
  $props = array(
    'bundle' => $base_conf['display']['bundle'], 
  );
  $info = entity_get_info($base_conf['entity_type']);
  $idcol = $info['entity keys']['id'];
  // apply tokens if requested for all properties and fields
  $propkeys = array_keys($base_conf['display']['properties']);
  $fieldkeys = array_keys($base_conf['display']['fields']);
  $token_targets = array_merge((array)$propkeys, (array)$fieldkeys);
  $token_keys = array('default', 'name', 'label', 'description');
  foreach ($token_targets as $token) {
    foreach ($token_keys as $key) {
      if (isset($base_conf['display']['properties'][$token][$key])) {
        $val = token_replace($base_conf['display']['properties'][$token][$key], array(), array('clear'=>TRUE));
        $base_conf['display']['properties'][$token][$key] = $val;
      }
      if (isset($base_conf['display']['fields'][$token][$key])) {
        $val = token_replace($base_conf['display']['fields'][$token][$key], array(), array('clear'=>TRUE));
        $base_conf['display']['fields'][$token][$key] = $val;
      }
      if ( ($key == 'default') and ($token <> 'bundle')) {
        // load up the props array in case this is a create request
        $props[$token] = $val;
      }
    }
  }
  //drupal_set_message("Final props array: <pre>" . print_r($props,1) . "</pre>");
  $op = 'add';
  // add special token support if a token is provided for the entity_id property
  //drupal_set_message("ID Col: base_conf['display']['properties'][ $idcol ]['default'] = " . $base_conf['display']['properties'][$idcol]['default']);
  if (intval($base_conf['display']['properties'][$idcol]['default']) > 0) {
    $base_conf['entity_id'] = intval($base_conf['display']['properties'][$idcol]['default']);
  }
  // TODO
  // should go through here and make or access formatters/handlers for each 
  // attached field type
  if (isset($base_conf['entity_id'])) {
    drupal_set_message("Retrieving entity $base_conf[entity_id] ");
    $entity = entity_load_single($base_conf['entity_type'], $base_conf['entity_id']);
    if ($entity) {
      $op = 'edit';
    }
  }
  $entity = is_object($entity) ? $entity : entity_create($base_conf['entity_type'], $props);

  // just grab the regular form for proof of concept
  // may later submit form_id to allow custom base forms
  $form_state['entity_type'] = $base_conf['entity_type'];
  if (strlen($base_conf['destination']) > 0) {
    $form_state['redirect'] = $base_conf['destination'];
  }
  $elements = entity_ui_get_form($base_conf['entity_type'], $entity, $op, $form_state);
  foreach ($base_conf['display']['properties'] as $prop) {
    if ($prop['hidden']) {
      $hidden[] = $prop['name'];
    }
    $format_keys = array('label', 'description');
    foreach ($format_keys as $key) {
      $elements[$prop['name']]['#' . $key] = $prop[$key];
    }
  }
  foreach ($base_conf['display']['fields'] as $field) {
    if ($field['hidden']) {
      $hidden[] = $field['name'];
    }
    $format_keys = array('label', 'description');
    foreach ($format_keys as $key) {
      $elements[$field['name']]['#' . $key] = $field[$key];
    }
  }
  foreach ($hidden as $hide) {
    $elements[$hide]['#type'] = 'hidden';
    $elements[$hide]['#theme'] = 'hidden';
    unset($elements[$hide]['#title']);
    unset($elements[$hide]['#description']);
  }
  //drupal_set_message("Elements[bundle]: <pre>" . print_r($elements['bundle'],1) . "</pre>");
  $form = drupal_render($elements);
  return $form;
}
